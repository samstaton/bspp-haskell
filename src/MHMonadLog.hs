{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MHMonadLog where

import Control.Monad.Trans.Writer
import Control.Monad.State
import Data.Monoid
import System.Random 
import Debug.Trace
import System.IO.Unsafe
import Control.Monad.Extra
import Numeric.Log
  
-- As programs run, they write scores (likelihoods) and we keep track of
-- the length of each run.
-- They also use randomness.
-- We consider this as a state with a stream of random numbers. 
newtype Meas a = Meas (WriterT (Product (Log Double),Sum Int) (State [Double]) a)
  deriving(Functor, Applicative, Monad)

-- Score weights the result, typically by the likelihood of an observation. 
score :: Double -> Meas ()
score r = Meas $ tell $ (Product $ (Exp . log)
                          $ (if r==0 then exp(-300) else r),Sum 0)

scorelog :: Double -> Meas ()
scorelog r = Meas $ tell $ (Product $ Exp
                          $ (if r==0 then exp(-300) else r),Sum 0)

-- Treating the state as a stream of seeds,
-- return a random number
getrandom :: State [Double] Double
getrandom = do
  ~(r:rs) <- get
  put rs
  return r

-- Sample draws a new sample, and increments the length of the current run.
sample :: Meas Double
sample = Meas $
       do r <- lift $ getrandom
          tell $ (Product 1,Sum 1)
          return r


----------
-- Inference engine 1: 
-- Likelihood weighted importance sampler
----------

-- Output a stream of weighted samples from a program. 
weightedsamples :: Meas a -> IO [(a,Product (Log Double))]
weightedsamples (Meas m) =
                    do let helper = do
                             (x, w) <- runWriterT m
                             rest <- helper
                             return $ (x,w) : rest
                       g <- getStdGen
                       let rs = randoms g
                       let (xws,_) = runState helper rs
                       return $ map (\(x,(w,i)) -> (x,w)) xws 

-- A likelihood weighted importance sampler.
-- lwis m n runs program m, n times, to get an empirical distribution.
-- and returns a stream of draws from that. 
lwis :: Meas a -> Int -> IO [a]
lwis m n =        do xws <- weightedsamples m
                     let xws' = take n $ accumulate xws 0
                     let max = snd $ last xws'
                     g <- getStdGen
                     let rs = (randoms g :: [Double])
                     return $ map (\r -> fst $ head $ filter (\(x,w)-> w >= (Product $ Exp $ log r) * max) xws') rs
accumulate ((x,w):xws) a = (x,w+a):(accumulate xws (w+a))
accumulate [] a = []


----------
-- Inference engine 2: 
-- Single-site Metropolis-Hastings
----------

-- categ rs r treats rs as a categorical distribution, and r as a seed in [0,1], and returns the draw.
categ :: [Double] -> Double -> Integer
categ rs r = let helper (r':rs') r'' i =
                          if r'' < r' then i else helper rs' (r''-r') (i+1)
           in helper rs r 0

-- Produce a stream of samples, together with their weights,
-- using single site Metropolis Hastings.
mh :: forall a. Meas a -> IO [(a,Product (Log Double))]
mh (Meas m) =
  do -- The function step takes a random seed (as) and produces a new one
     -- according to the Metropolis Hastings acceptance ratio.
     -- NB There are two kinds of randomness in the step function.
     -- . The seed as, which is the source of randomness for simulating the
     --   program m.
     -- . The randomness needed in the MH step, which is taken care of in
     --   the state monad using getrandom.
     let step :: [Double] -> State [Double] [Double]
         step as = do
           -- Get the weight w and length l of the run with given seed as.
           -- NB given the seed as, the simulation of m is deterministic.
           let ((_, (w,l)),_) =
                 runState (runWriterT m) as 
           -- Randomly pick which site to change
           r <- getrandom
           let i = categ (replicate (fromIntegral $ getSum l)
                          (1/(fromIntegral $ getSum l))) r 
           -- Replace that site with a new random choice.
           -- Put together the new candidate seed as', which is
           -- the same as before except at position i
           r' <- getrandom
           let as' =
                 (let (as1,_:as2) = splitAt (fromIntegral i) as in
                    as1 ++ r' : as2)
           -- Rerun the model with the new seed as', to get a new
           -- weight w' and length l'.
           let ((_, (w',l')),_) =
                 runState (runWriterT m) (as') 
           -- MH acceptance ratio. This is the probability of either 
           -- returning the new seed or the old one. 
           let ratio = getProduct w' * (fromIntegral $ getSum l)
                       / (getProduct w * (fromIntegral $ getSum l'))
           r'' <- getrandom
           if r'' < (min 1 ((exp . ln) ratio)) then return as' else return as
     -- Top level: produce a stream of samples.
     -- Split the random number generator in two
     -- One part is used as the seed for the simulation,
     -- and one part is used for the randomness in the MH algorithm. 
     g <- getStdGen
     let (g1,g2) = split g
     -- Now run step over and over to get a stream of seeds.
     let (samples,_) = runState (iterateM step (randoms g1)) (randoms g2)
     -- The stream of seeds is used to produce a stream of result/weight pairs. 
     return $ map (\(x,(w,l)) -> (x,w)) 
       $ map (\as -> fst $ runState (runWriterT m) as)
       $ samples
     
