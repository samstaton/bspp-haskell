module Fruit where

import MHMonad
import Distr

-- Simple fruit puzzle from BSPP lectures

data Fruit = Apple | Orange
data Bin = RedBin | BlueBin deriving Eq

-- We have two bins of fruit.
-- In each bin, we know the ratio of apples to oranges.
prob :: Bin -> Fruit -> Double
prob RedBin Apple = 2/8
prob RedBin Orange = 6/8
prob BlueBin Apple = 3/4
prob BlueBin Orange = 1/4

-- Fruit (f) was taken from a bin. Prior probability of red bin in 1/6.
model :: Fruit -> Meas Bin
model f = do
  bin <- categoricalTagged [(RedBin,1/6),(BlueBin,5/6)]
  score $ prob bin f
  return $ bin

-- Use LWIS to get a posterior distribution on bins.
-- Look at which proportion are blue. 
test =
  do
    bins <- lwis (model Orange) 100000
    let blues = length $ filter (== BlueBin) $ take 10000 $ bins
    print $ show $ (fromIntegral blues) / 10000

