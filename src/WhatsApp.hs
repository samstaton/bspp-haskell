module WhatsApp where
import MHMonad
import Distr
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Diagrams
import Graphics.Rendering.Chart.State
import Data.Colour
import Data.Colour.Names
import Data.Default.Class
import Control.Lens
import Data.Monoid 

-- A very simple first probabilistic program from the BSPP lectures

-- Is it the Sunday?
model = do
  -- Prior belief: it is Sunday with prob. 2/7
  sunday <- bernoulli (1/7)
  -- I get fewer messages on Sundays
  let rate = if sunday then 3 else 10 
  -- observe 4 messages
  score $ poissonPdf rate 4
  return sunday

test = do
  xws <- weightedsamples model
  let xws' = map (\(x,w) -> (x, w)) xws
  _ <- renderableToFile def "whatsapp-histogram.svg"
    (chart (toHistogram (take 100000 xws')))
  return ()
  


-- functions for plotting a histogram
totalweight :: [(a,Double)] -> Double
totalweight rxs = sum $ map (\(_,r) -> r) rxs

toHistogram :: [(Bool,Double)] -> [Double]
toHistogram rxs = 
  map (\x' -> (sum $ map (\(x,r) -> if x == x' then r else 0) rxs) / (totalweight rxs)) [True,False]

chart :: [Double] -> Renderable ()
chart rs = toRenderable layout
 where
  layout =
        layout_title .~ "Histogram" ++ btitle
      $ layout_title_style . font_size .~ 10
      $ layout_x_axis . laxis_generate .~ autoIndexAxis alabels
      $ layout_y_axis . laxis_generate .~ scaledAxis def (0,1)
      $ layout_left_axis_visibility . axis_show_ticks .~ False
      $ layout_plots .~ [ plotBars bars2 ]
      $ def :: Layout PlotIndex Double

  bars2 = plot_bars_titles .~ [""]
      $ plot_bars_values .~ addIndexes (map (\x -> [x]) rs) -- [45,30],[30,20],[70,25]]
      $ plot_bars_spacing .~ BarsFixGap 30 5
      $ plot_bars_item_styles .~ map mkstyle (cycle defaultColorSeq)
      $ def

  alabels = ["sunday","not sunday"]

  btitle = "" 
  bstyle = Just (solidLine 1.0 $ opaque black) 
  mkstyle c = (solidFillStyle c, bstyle)

