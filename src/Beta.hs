{-# LANGUAGE ScopedTypeVariables     #-}

module Beta where
import MHMonad
import Distr
import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Diagrams
import Graphics.Rendering.Chart.State
import Data.Colour
import Data.Colour.Names
import Data.Default.Class
import Control.Lens
import Data.Monoid 

-- Simple examples to illustrate beta distributions.
-- What is the probability that an unknown coin will return heads twice?

model1 = do
  p ::Double <- sample
  b1 <- bernoulli p
  b2 <- bernoulli p
  return (b1 == b2)

inttolist 0 = []
inttolist n = () : (inttolist (n-1))


test1 = do
  xws <- weightedsamples model1
  let xws' = map (\(x,w) -> (x, w)) xws
  _ <- renderableToFile def "beta-histogram-1.svg"
    (chart (toHistogram (take 10000 xws')))
  putStr $ show $ toHistogram (take 10000 xws')
  return ()

test2 = do 
  xws <- weightedsamples $ poisson 1
  let xws' = map (\(x,w) -> (x, w)) xws
  putStr "Hello"
  putStr $ show $ sum (map (\(x,w) -> x) (take 10000 xws'))
  putStr $ show $ toHistogramInt (take 10000 xws')
  putStr $ show $ sum $ toHistogramInt (take 10000 xws')
  _ <- renderableToFile def "swaraj-histogram-1.svg"
    (chart (toHistogramInt (take 10000 xws')))
--  putStr $ show $ toHistogramInt (take 10000 xws')
  return ()


-- functions for plotting a histogram
totalweight :: [(a,Double)] -> Double
totalweight rxs = sum $ map (\(_,r) -> r) rxs

toHistogram :: [(Bool,Double)] -> [Double]
toHistogram rxs = 
  map (\x' -> (sum $ map (\(x,r) -> if x == x' then r else 0) rxs) / (totalweight rxs)) [True,False]

toHistogramInt :: [(Integer,Double)] -> [Double]
toHistogramInt rxs = 
  map (\x' -> (sum $ map (\(x,r) -> if x == x' then r else 0) rxs) / (totalweight rxs)) [0..20]

chart :: [Double] -> Renderable ()
chart rs = toRenderable layout
 where
  layout =
        layout_title .~ "Histogram" ++ btitle
      $ layout_title_style . font_size .~ 10
      $ layout_x_axis . laxis_generate .~ autoIndexAxis alabels
      -- $ layout_y_axis . laxis_override .~ axisGridHide
      $ layout_y_axis . laxis_generate .~ scaledAxis def (0,1)
      $ layout_left_axis_visibility . axis_show_ticks .~ False
      $ layout_plots .~ [ plotBars bars2 ]
      $ def :: Layout PlotIndex Double

  bars2 = plot_bars_titles .~ [""]
      $ plot_bars_values .~ addIndexes (map (\x -> [x]) rs) -- [45,30],[30,20],[70,25]]
--      $ plot_bars_style .~ BarsClustered
      $ plot_bars_spacing .~ BarsFixGap 30 5
      $ plot_bars_item_styles .~ map mkstyle (cycle defaultColorSeq)
      $ def

  alabels = ["two heads","not two heads"]

  btitle = "" 
  bstyle = Just (solidLine 1.0 $ opaque black) 
  mkstyle c = (solidFillStyle c, bstyle)

