# bspp

This is a simple Metropolis-Hastings implementation in Haskell together with some examples. It is designed to illustrate the basics of Metropolis-Hastings simulation for the Oxford CS course on probabilistic programming ([2021 link](https://www.cs.ox.ac.uk/teaching/courses/2021-2022/SPP/index.html)). Some advantages are that the M-H implementation is short and (hopefully) readable, and the language is typed. 

To try different examples, change ``app/Main.hs``
or use ``stack ghci``.
The source is in ``src/`` .

## Simple probabilistic programming library

* [``MHMonad.hs``](src/MHMonad.hs) contains the likelihood weighted importance sampling algorithm (lwis) and single-site Metropolis-Hastings. This provides an interface using "sample" (which draws a uniform number in [0,1]) and "score" which weights a trace. 
* [``Distr.hs``](src/Distr.hs) contains common distributions such as normal distributions etc..
* ``*log.hs`` is similar but using log numbers, which works much better with the small numbers that often arise.

## Examples

* [``Voting.hs``](src/Voting.hs) is the simple example of election prediction from the lectures.
* [``Regression.hs``](src/Regression.hs) contains the linear regression problem from the lectures.
* [``Mixture.hs``](src/Mixture.hs) contains the mixture models and clustering problems from the lectures.
* [``Smc.hs``](src/Smc.hs) contains the Kalman filter and Sequential Monte Carlo examples from the lectures.

Not really covered in the lectures in Weeks 7 and 8 but in case you want to experiment: 

* [``Beta.hs``](src/Beta.hs), a very simple model
* [``Bus.hs``](src/Bus.hs), a variation on the WhatsApp example from lectures
* [``Fruit.hs``](src/Fruit.hs), the very simple Fruit example from lectures
* [``Physics.hs``](src/Physics.hs), a variation on the bouncing balls example
* [``ProgInduction.hs``](src/ProgInduction.hs), a simple program induction example
* [``WhatsApp``](src/WhatsApp.hs), the very simple WhatsApp example from lectures

## Installation

The system uses Haskell stack.
You need to [install stack](https://docs.haskellstack.org/en/v1.1.2/install_and_upgrade/) first if you want to use the system in the
standard way.

To build, type
``stack build``.
This may take some time (>1 hour) if it is your first time ever using stack.

To run, type
``stack run`` or ``stack ghci``.  

## Some pointers

* The ideas in this library have been used in many other systems. For a fuller survey, see the course lecture notes if you can access them, or [An Introduction to Probabilistic Programming
Jan-Willem van de Meent, Brooks Paige, Hongseok Yang, Frank Wood](https://arxiv.org/abs/1809.10756).

* The [lazyppl library](https://bitbucket.org/samstaton/lazyppl) used this one as a starting point, but allows full use of Haskell's laziness. 
